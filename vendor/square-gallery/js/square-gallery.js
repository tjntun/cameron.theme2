/**
 * SquareGallery 2
 *
 * © Oduvan Studio 2015
 *
 */
;(function(global){
    'use strict';


    function addEvent(el, eventName, callback){
        if(global.addEventListener){
            el.addEventListener(eventName, callback);
        }else{
            el.attachEvent('on' + eventName, callback);
        }
    }


    /**
     *
     * @param [options] - customize the display gallery
     * @constructor
     */
    function SquareGallery(options){
        options || (options = {});

        if(options.container == null){
            throw 'SquareGallery: options.el is a require';
        }

        this.container = typeof options.container === 'string' ? document.getElementById(options.container) : options.container;

        this.galleryProperties =    this._getPropertiesFromDOM();

        this.qtyColumns =           options.qtyColumns || 2;
        this.textSquares =          options.textSquares || false;
        this.containerBGColor =     options.containerBGColor || '#fff';
        this.photoCaptions =        options.photoCaptions || false;
        this.captionType =          options.captionType || 1;
        this.links =                options.links || false;

        this.qtyImages =            this.galleryProperties.images.length;
        this.columnDivs =           [];
        this.imageCounter =         0;

        this.sequence =             this.textSquares ? this._generateTextSequence() : this._generateSequence();

        this._generateColumns();
    }

    /**
     * Import DOM properies into JSON object
     * @returns {{path: string, images: Array}}
     * @private
     */
    SquareGallery.prototype._getPropertiesFromDOM = function(){
        var nodes = this.container.childNodes,
            result = {
                'path':'./',
                images:[]
            };

       
        for(var i = 0, l = nodes.length; i < l; i++){

            if(nodes[i].nodeName === 'IMG'){
                var item = {};

                if(nodes[i].getAttribute('data-src')){
                    item.file = nodes[i].getAttribute('data-src');
                }

                if(nodes[i].getAttribute('data-thumb')){
                    item.thumb = nodes[i].getAttribute('data-thumb');
                }

                if(nodes[i].getAttribute('data-name')){
                    item.name = nodes[i].getAttribute('data-name');
                }

                if(nodes[i].getAttribute('data-description')){
                    item.description = nodes[i].getAttribute('data-description');
                }

                if(nodes[i].getAttribute('data-href')){
                    item.href = nodes[i].getAttribute('data-href');
                }

                result.images.push(item);
            }
        }

        this.container.innerHTML = '';

        return result;
    };

    /**
     * Generate div-columns for the galery
     * @private
     */
    SquareGallery.prototype._generateColumns = function(){
        var div,
            col = 0;

        for(var colsIterator = 0; colsIterator < this.qtyColumns; colsIterator++){
            div = document.createElement('div');
            div.className = 'sqr-col';
            if(this.containerBGColor){
                div.style.backgroundColor = this.containerBGColor;
            }
            div.style.width = (100 / this.qtyColumns) + '%';
            this.container.appendChild(div);
            this.columnDivs.push(div);
        }

        for(var sequenceIterator = 0, l = this.sequence.length; sequenceIterator < l; sequenceIterator++){
            col = col >= this.qtyColumns ? 0 : col;
            this.textSquares ? this._insertTextBlock(this.sequence[sequenceIterator], this.columnDivs[col]) : this._insertImage(this.sequence[sequenceIterator], this.columnDivs[col]);
            col++;
        }
    };

    /**
     * Inserts the image into the container
     * @param imgCount
     * @param parent
     * @private
     */
    SquareGallery.prototype._insertImage = function(imgCount, parent){
        var imgEl, imgObj, p, that = this;

        for(var i = 0, l = imgCount; i < l; i ++){
            imgObj = this.galleryProperties.images[this.imageCounter];

            imgEl = document.createElement('div');
            imgEl.className = imgCount === 3 && i === 0 ? 'sqr-square middle' : imgCount === 1 ? 'sqr-square' : 'sqr-square small';



            (function(imgEl, imgObj, i){
                var image = new Image(),
                    preloader = that._setPreloader(imgEl);

                image.onload = function(){
                    imgEl.style.backgroundImage = 'url("' + image.src + '")';

                    that.galleryProperties.images[i].width = image.width;
                    that.galleryProperties.images[i].height = image.height;

                    imgEl.removeChild(preloader);

                    image = null;
                };
                image.src = imgObj.thumb || imgObj.src;
            })(imgEl, imgObj, this.imageCounter);



            if(this.photoCaptions && imgObj.name != null){
                switch (this.captionType) {
                    case 1:
                        imgEl.className = imgEl.className + ' sqr-caption1';
                        imgEl.setAttribute('data-caption', imgObj.name);
                        break;
                    case 2:
                        imgEl.className = imgEl.className + ' sqr-caption2';
                        p = document.createElement('p');
                        p.appendChild(document.createTextNode(imgObj.name));
                        imgEl.appendChild(p);
                        break;
                    case 3:
                        imgEl.className = imgEl.className + ' sqr-caption3';
                        p = document.createElement('p');
                        p.appendChild(document.createTextNode(imgObj.name));
                        imgEl.appendChild(p);
                        break;
                    case 4:
                        imgEl.className = imgEl.className + ' sqr-caption4';
                        imgEl.setAttribute('data-caption', imgObj.name);
                        break;
                    case 5:
                        imgEl.className = imgEl.className + ' sqr-caption5';
                        imgEl.setAttribute('data-caption', imgObj.name);
                        break;
                    default:
                        break;
                }
            }

            parent.appendChild(imgEl);

            this._setAction(imgEl, imgObj.href != null && this.links ? imgObj.href : this.imageCounter, this.links);

            this.imageCounter++;
        }
    };

    /**
     * Set gif preloader to load images
     * @param el
     * @returns {Element}
     * @protected
     */
    SquareGallery.prototype._setPreloader = function(el){
        var preloader = document.createElement('img');
        //preloader.src = 'build/css/img/preloader.gif';
        preloader.className = 'sqr-preloader';
        el.appendChild(preloader);

        return preloader;
    };

    /**
     * Inserts the text blocks into the container
     * @param imgCount
     * @param parent
     * @private
     */
    SquareGallery.prototype._insertTextBlock = function(imgCount, parent){
        var imgEl, textBlock, p, h3, imgObj, that = this;

        for(var i = 0; i < imgCount; i ++){
            imgObj = this.galleryProperties.images[this.imageCounter];

            imgEl = document.createElement('div');
            imgEl.className = imgCount === 2 && i === 1  ? 'sqr-text-square small right' : imgCount === 2 ? 'sqr-text-square small' : 'sqr-text-square';

            (function(imgEl, imgObj, i){
                var image = new Image(),
                    preloader = that._setPreloader(imgEl);

                image.onload = function(){
                    imgEl.style.backgroundImage = 'url("' + image.src + '")';

                    that.galleryProperties.images[i].width = image.width;
                    that.galleryProperties.images[i].height = image.height;

                    imgEl.removeChild(preloader);

                    image = null;
                };
                image.src = imgObj.thumb || imgObj.src;
            })(imgEl, imgObj, this.imageCounter);

            textBlock = document.createElement('div');
            textBlock.className = 'sqr-text-block';

            h3 = document.createElement('h3');
            h3.appendChild(document.createTextNode(imgObj.name || ''));
            textBlock.appendChild(h3);

            p = document.createElement('p');
            p.appendChild(document.createTextNode(imgObj.description || ''));
            textBlock.appendChild(p);

            imgEl.appendChild(textBlock);
            parent.appendChild(imgEl);

            this._setAction(imgEl, imgObj.href != null && this.links ? imgObj.href : this.imageCounter, this.links);

            this.imageCounter++;
        }
    };

    /**
     * Shuffle the squares with photos
     * @param arr
     * @returns {Array}
     * @private
     */
    SquareGallery.prototype._shuffle = function(arr) {
        var shuffled = [], rand;
        for(var index = 0, l = (arr).length; index < l; index++){
            var value = (arr)[index];
            if (index == 0) {
                shuffled[0] = value;
            } else {
                rand = Math.floor(Math.random() * (index + 1));
                shuffled[index] = shuffled[rand];
                shuffled[rand] = value;
            }
        }
        return shuffled;
    };

    /**
     * Removing the recurrence of the same blocks with images
     * @param arr
     * @returns {Array}
     * @private
     */
    SquareGallery.prototype._moveRepeats = function(arr) {
        var arrayIterator = 0,
            maxIteration = 20,
            resultArr = [];

        while(arr.length){
            if(arrayIterator === arr.length){
                arrayIterator = 0;
            }

            if(maxIteration < 0){
                break;
            }

            if(resultArr[resultArr.length-1] !== arr[arrayIterator]){
                resultArr.push(arr[arrayIterator]);
                arr.splice(arrayIterator, 1)
            }else if(resultArr[0] !== arr[arrayIterator]){
                resultArr.unshift(arr[arrayIterator]);
                arr.splice(arrayIterator, 1)
            }else{
                arrayIterator++
            }

            maxIteration--;
        }

        for(var i = 0, l = arr.length; i < l; i++){
            resultArr.push(arr[i]);
        }

        return resultArr;
    };

    /**
     * Creates a sequence of images of different sizes and adjusts them by the number of square
     * @returns {Array}
     * @private
     */
    SquareGallery.prototype._generateSequence = function(){
        var array = [4, 3, 1],
            maxNum = this.qtyImages,
            sequence = [],
            col = this.qtyColumns,
            i = 0;

        while(maxNum > 0){
            i = i === 3 ? 0 : i;

            if(maxNum > array[i]){
                sequence.push(array[i]);
                maxNum -= array[i];
            }else{
                if(maxNum === 2){
                    if(sequence[2]){
                        sequence[2] = 3;
                    }else{
                        sequence.push(1);
                        sequence.push(1);
                    }
                }else{
                    sequence.push(maxNum);
                }
                maxNum = 0;
            }
            i++;
        }

        var extraCol = sequence.length % col,
            saveNum = 0;

        while(extraCol){
            var randNum = Math.floor(Math.random() * (sequence.length + 1));
            if(sequence[sequence.length - 1] === 4){
                sequence[sequence.length - 1] = 3;
                sequence.push(1);
            }else if(sequence[randNum] === 3 && sequence[sequence.length - 1] === 1 && sequence.length > 4){
                sequence[randNum] = 4;
                sequence.pop();
            }else if(sequence[randNum] === 1 && sequence[sequence.length - 1] === 3 && sequence.length > 4){
                sequence[randNum] = 4;
                sequence.pop();
            }else if(sequence[randNum] === 4){
                sequence[randNum] = 3;
                sequence.push(1);
            }

            if(saveNum === 1000){
                break;
            }
            saveNum++;
            extraCol = sequence.length % col;
        }

        return this._moveRepeats(sequence);
    };

    /**
     * Creates a sequence of images with text blocks of different sizes and adjusts them by the number of square
     * @returns {Array}
     * @private
     */
    SquareGallery.prototype._generateTextSequence = function(){
        var array = [2,1,1,2],
            maxNum = this.qtyImages,
            sequence = [],
            col = this.qtyColumns;


        var i = 0;
        while(maxNum > 0){
            i = i === array.length ? 0 : i;

            if(maxNum >= array[i]){
                sequence.push(array[i]);
                maxNum -= array[i];
            }else{
                sequence.push(maxNum);
                maxNum = 0;
            }

            i++;
        }

        var extraCol = sequence.length%col;

        var saveNum = 0;
        while(extraCol){
            var randNum = Math.floor(Math.random() * (sequence.length + 1));
            if(sequence[sequence.length-1] === 2){
                sequence[sequence.length-1] = 1;
                sequence.push(1);
            }else if(sequence[randNum] === 1 && sequence[sequence.length-1] === 1 && sequence.length>4){
                sequence[randNum] = 2;
                sequence.pop();
            }else if(sequence[randNum] === 2){
                sequence[randNum] = 1;
                sequence.push(1);
            }

            if(saveNum === 100){
                break;
            }

            saveNum++;
            extraCol = sequence.length%col;
        }

        return sequence;
    };

    /**
     * Bind click-action on the image
     * @param el
     * @param value
     * @param isLink
     * @private
     */
    SquareGallery.prototype._setAction = function(el, value, isLink){

        var that = this;

        if(isLink){
            addEvent(el, 'click', function(){
                global.location = value;
            });
        }else{
            addEvent(el, 'click', function(){
                new global.Lightbox(el, value, that.galleryProperties);
            });
        }
    };

    global.SquareGallery = SquareGallery;
})(this);
/**
 * SquareGallery 2
 *
 * © Oduvan Studio 2015
 *
 */
;(function(global){

    /**
     * Cross browser ScrollY
     * @returns {Number}
     */
    function getScrollTop(){
        return global.pageYOffset ? global.pageYOffset : document.documentElement.clientHeight ? document.documentElement.scrollTop : document.body.scrollTop
    }

    /**
     * Cross browser addEventListener
     * @param el
     * @param eventName
     * @param callback
     */
    function addEvent(el, eventName, callback){
        if(global.addEventListener){
            el.addEventListener(eventName, callback);
        }else{
            el.attachEvent('on' + eventName, callback);
        }
    }

    /**
     * Cross browser removeEventListener
     * @param el
     * @param eventName
     * @param callback
     */
    function removeEvent(el, eventName, callback){
        if(global.addEventListener){
            el.removeEventListener(eventName, callback);
        }else{
            el.detachEvent('on' + eventName, callback);
        }
    }

    /**
     * Lightbox constructor
     * @param el
     * @param imgNumber
     * @param properties
     * @constructor
     */
    function Lightbox(el, imgNumber, properties){

        this.imgNumber = imgNumber;
        this.imgWidth = this.imgHeight = 0;

        this.container = (function(){ return el.cloneNode(false)})();

        this.blackOut = document.createElement('div');
        this.blackOut.className = 'sqr-blackout';
        document.body.appendChild(this.blackOut);

        this.properties = properties;

        this._initAnimation(el.getBoundingClientRect());

        this._renderControls();

        this._bindEvents();

        this._updatePager();

    }

    /**
     * Init start animation
     * @param parentRect
     * @private
     */
    Lightbox.prototype._initAnimation = function(parentRect){
        var that = this,
            rect = [],
            image = that.properties.images[this.imgNumber];


        this.container.style.top = (parentRect.top + getScrollTop()) + 'px';
        this.container.style.left = parentRect.left + 'px';
        this.container.style.width = (parentRect.width || (parentRect.right - parentRect.left)) + 'px';
        this.container.style.height = (parentRect.height || (parentRect.bottom - parentRect.top)) + 'px';
        this.container.className = 'sqr-lightbox-container';
        document.body.appendChild(this.container);

        global.setTimeout(function(){
            rect = that._fit.call(that, image.width, image.height);

            that.imgWidth = rect[0];
            that.imgWidth = rect[1];

            that.container.style.width = rect[0] + 'px';
            that.container.style.height = rect[1] + 'px';
            that.container.style.left = rect[2] + 'px';
            that.container.style.top = (rect[3] + getScrollTop()) + 'px';

            that.blackOut.className = that.blackOut.className + ' visible';

            that._loadImage.call(that, that.imgNumber);
        }, 5);


        global.setTimeout(function(){
            that.container.className = that.container.className + ' no-animation';
        }, 500);

        document.body.style.overflow = 'hidden';
    };

    /**
     * Bind buttons and click event on the lightbox
     * @private
     */
    Lightbox.prototype._bindEvents = function(){
        var that = this;

        this._destroyByKey = function(event){
            var code = event.charCode ? event.charCode : event.keyCode;
            code = parseInt(code);

            if(code === 39){
                that._next.call(that);
            }else if(code === 37){
                that._prev.call(that);
            }else if(code === 27){
                that._destroy.call(that);
            }
        };

        addEvent(document, 'keydown', this._destroyByKey);

        var destroy = function(event){
            if(event.stopPropagation) {
                event.stopPropagation();
            } else {
                event.cancelBubble = true;
            }

            that._destroy.call(that);
        };

        addEvent(this.blackOut, 'click', destroy);


        addEvent(document.getElementById('sqr-close'), 'click', destroy);

        addEvent(document.getElementById('sqr-left'), 'click', function(event){
            if(event.stopPropagation) {
                event.stopPropagation();
            } else {
                event.cancelBubble = true;
            }

            that._prev.call(that);
        });
        addEvent(document.getElementById('sqr-right'), 'click', function(event){
            if(event.stopPropagation) {
                event.stopPropagation();
            } else {
                event.cancelBubble = true;
            }
            
            that._next.call(that);
        });

        addEvent(window, 'resize', function(){
            that._resize.call(that);
        });
    };

    /**
     * Render lightbox pagination info
     * @private
     */
    Lightbox.prototype._updatePager = function(){
        this.container.setAttribute('data-pager', (this.imgNumber + 1) + ' / ' + this.properties.images.length);
    };

    /**
     * Destroy lightbox animation (unbind key events, remove DOM nodes, etc)
     * @private
     */
    Lightbox.prototype._destroy = function(){
        removeEvent(document, 'keydown', this._destroyByKey);
        document.body.removeChild(this.container);
        document.body.removeChild(this.blackOut);
        document.body.style.overflow = '';
    };

    /**
     * Resize lightbox when the user resize window
     * @private
     */
    Lightbox.prototype._resize = function(){
        var newRect = this._fit(this.imgWidth, this.imgHeight);

        this.container.style.width = newRect[0] + 'px';
        this.container.style.height = newRect[1] + 'px';
        this.container.style.left = newRect[2] + 'px';
        this.container.style.top = (newRect[3] + getScrollTop()) + 'px';
    };


    /**
     * Loading gallery image on the lightbox
     * @private
     */
    Lightbox.prototype._loadImage = function(){
        var img, that = this, mainImage = document.getElementById('sqr-image');

        img = new Image();

        img.onload = function(){
            if(!that.container){
                return
            }

            that.imgWidth = img.width;
            that.imgHeight = img.height;

            var rect = that._fit.call(that, img.width, img.height);

            that.container.style.width = rect[0] + 'px';
            that.container.style.height = rect[1] + 'px';
            that.container.style.left = rect[2] + 'px';
            that.container.style.top = (rect[3] + getScrollTop()) + 'px';

            var imgEl = document.createElement('img');
            imgEl.src = img.src;
            imgEl.className = 'sqr-image';
            imgEl.id = 'sqr-image';

            if(mainImage){
                that.container.removeChild(mainImage);
            }

            that.container.appendChild(imgEl);
            that.container.style.backgroundImage = 'url()';

            that._updatePager();

            that._removePreloader();
            img = null;
        };

        img.src = this.properties.images[this.imgNumber].file;

        this._setPreloader();
    };

    /**
     * Set preloader when image loading
     * @private
     */
    Lightbox.prototype._setPreloader = function(){

        if(document.getElementById('sqr-preloader')){
            return;
        }

        var preloader = document.createElement('img');
        //preloader.src = 'build/css/img/preloader.gif';
        preloader.className = 'sqr-preloader';
        preloader.id = 'sqr-preloader';
        this.container.appendChild(preloader);
    };

    /**
     * Remove preloader when image loaded
     * @private
     */
    Lightbox.prototype._removePreloader = function(){
        if(!document.getElementById('sqr-preloader')){
            return;
        }
        this.container.removeChild(document.getElementById('sqr-preloader'));
    };

    /**
     * Render arrow and exit controls
     * @private
     */
    Lightbox.prototype._renderControls = function(){
        var closeControl = document.createElement('span'),
            leftArrow = document.createElement('span'),
            rightArrow = document.createElement('span');

        closeControl.className = 'sqr-icon-close';
        closeControl.id = 'sqr-close';
        leftArrow.className = 'sqr-icon-left';
        leftArrow.id = 'sqr-left';
        rightArrow.className = 'sqr-icon-right';
        rightArrow.id = 'sqr-right';

        this.blackOut.appendChild(closeControl);
        this.blackOut.appendChild(leftArrow);
        this.blackOut.appendChild(rightArrow);
    };

    /**
     * Go to the next image
     * @private
     */
    Lightbox.prototype._next = function(){
        if(this.imgNumber === this.properties.images.length-1){
            return
        }

        this.imgNumber++;
        this._loadImage();
    };

    /**
     * Go to the previous image
     * @private
     */
    Lightbox.prototype._prev = function(){
        if(this.imgNumber === 0){
            return
        }
        this.imgNumber--;
        this._loadImage();
    };

    /**
     * Fit image to the screen
     * @param width
     * @param height
     * @private
     */
    Lightbox.prototype._fit = function(width, height){
        var innerW = global.innerWidth || document.documentElement.clientWidth || document.body.clientWidth,
            innerH = global.innerHeight || document.documentElement.clientHeight || document.body.clientHeight,
            SCREEN_WIDTH = innerW * 0.05 < 55 ? innerW - 110 : innerW * 0.9,
            SCREEN_HEIGHT = innerH * 0.05 < 30 ? innerH - 60 : innerH * 0.9;

        if(width < SCREEN_WIDTH && height < SCREEN_HEIGHT){
            return this._fitOriginal(width, height);
        }else if(width/height > SCREEN_WIDTH/SCREEN_HEIGHT){
            return this._fitWidth(width, height);
        }else if(width/height < SCREEN_WIDTH/SCREEN_HEIGHT){
            return this._fitHeight(width, height);
        }
    };

    /**
     * Fit the size of the original image to the screen
     * @param width
     * @param height
     * @returns {*[]}
     * @private
     */
    Lightbox.prototype._fitOriginal = function(width, height){
        var innerW = global.innerWidth || document.documentElement.clientWidth || document.body.clientWidth,
            innerH = global.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

        return [width, height, (innerW - width)/2, (innerH - height)/2];
    };

    /**
     * Fit image to the screen in width
     * @param width
     * @param height
     * @returns {*[]}
     * @private
     */
    Lightbox.prototype._fitWidth = function(width, height){
        var innerW = global.innerWidth || document.documentElement.clientWidth || document.body.clientWidth,
            innerH = global.innerHeight || document.documentElement.clientHeight || document.body.clientHeight,
            cropWidth = innerW * 0.05 < 55 ? innerW - 110 : innerW * 0.9,
            cropMarginLeft = innerW * 0.05 < 55 ? 55 : innerW * 0.05,
            cropHeight = Math.round((cropWidth / width) * height),
            cropMarginTop = Math.abs((innerH - cropHeight) / 2);

        return [cropWidth, cropHeight, cropMarginLeft, cropMarginTop];
    };

    /**
     * Fit image to the screen in height
     * @param width
     * @param height
     * @returns {*[]}
     * @private
     */
    Lightbox.prototype._fitHeight = function(width, height){
        var innerW = global.innerWidth || document.documentElement.clientWidth || document.body.clientWidth,
            innerH = global.innerHeight || document.documentElement.clientHeight || document.body.clientHeight,
            cropHeight = innerH * 0.05 < 30 ? innerH - 60 : innerH * 0.9,
            cropMarginTop = innerH * 0.05 < 30 ? 30 : innerH * 0.05,
            cropWidth = Math.round((cropHeight / height) * width),
            cropMarginLeft = Math.abs((innerW - cropWidth) / 2);

        return [cropWidth, cropHeight, cropMarginLeft, cropMarginTop];
    };

    global.Lightbox = Lightbox;

})(this);